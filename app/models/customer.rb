class Customer < ApplicationRecord
  has_many :products

  scope :lahat_ng_5, -> (edad) { where(age: edad) }
  scope :si_one, -> { find_by(id: 1) }

  validates :name, :age, presence: true
  validates :age, numericality: true
end
