class Product < ApplicationRecord
  belongs_to :customer
  validates :name, :price, presence: true
  validates :price, numericality: true
end
